package nz.geek.q.joist.robot;

import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOService;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.IBinder;

public class JoistService extends IOIOService {
	private static final String STOP_ACTION = "stop";

	// TODO: Set maximum capacity?
	private final BlockingQueue<Command> commandQueue = new LinkedBlockingQueue<>();

	@Override
	public void onCreate() {
		super.onCreate();
		NetworkClient client = new NetworkClient(commandQueue);
		new Thread(client).start();
	}

	@Override
	protected IOIOLooper createIOIOLooper() {
		return new Looper(commandQueue);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		if (intent != null && intent.getAction() != null && intent.getAction().equals(STOP_ACTION)) {
			// User clicked the notification. Need to stop the service.
			nm.cancel(0);
			stopSelf();
		} else {
			// Service starting. Create a notification.
			Notification notification = new Notification.Builder(this)
				.setSmallIcon(R.drawable.ic_launcher)
				.setTicker("IOIO service running")
				.setContentTitle("IOIO Service")
				.setContentText("Click to stop")
				.setContentIntent(PendingIntent.getService(this, 0, new Intent(STOP_ACTION, null, this, this.getClass()), 0))
				.setOngoing(true)
				.build();
			nm.notify(0, notification);
		}
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}
