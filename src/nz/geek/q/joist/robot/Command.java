package nz.geek.q.joist.robot;

/**
 * A command for the robot to drive in a particular direction for a certain amount of time.
 */
public class Command {
	public final Direction direction;
	public final float speed;
	public final long duration;

	public Command(Direction direction, float speed, long duration) {
		this.direction = direction;
		this.speed = speed;
		this.duration = duration;
	}

	@Override
	public String toString() {
		return direction + " at " + speed + " for " + duration + "ms";
	}
}
