package nz.geek.q.joist.robot;

import static nz.geek.q.joist.robot.Config.MOTOR_PWM_FREQUENCY;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.IOIO;
import ioio.lib.api.PwmOutput;
import ioio.lib.api.exception.ConnectionLostException;

public class Motor {
	private final PwmOutput enable;
	private final DigitalOutput forward;
	private final DigitalOutput reverse;

	public Motor(IOIO ioio, int enablePin, int forwardPin, int reversePin) throws ConnectionLostException {
		enable = ioio.openPwmOutput(enablePin, MOTOR_PWM_FREQUENCY);
		forward = ioio.openDigitalOutput(forwardPin);
		reverse = ioio.openDigitalOutput(reversePin);
	}

	public void turn(boolean forwards, float speed) throws ConnectionLostException {
		enable.setDutyCycle(speed);
		forward.write(forwards);
		reverse.write(!forwards);
	}

	public void brake() throws ConnectionLostException {
		enable.setDutyCycle(1);
		forward.write(false);
		reverse.write(false);
	}
	public void neutral() throws ConnectionLostException {
		enable.setDutyCycle(0);
		forward.write(false);
		reverse.write(false);
	}
}
