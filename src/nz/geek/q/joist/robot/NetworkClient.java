package nz.geek.q.joist.robot;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;

import android.util.Log;

/**
 * Client to connect to the server over TCP, decode commands and send them to the command queue.
 */
public class NetworkClient implements Runnable {
	private final static String TAG = "NetworkClient";

	private final static Map<String, Direction> DIRECTION_LOOKUP = new HashMap<>();

	static {
		DIRECTION_LOOKUP.put("f", Direction.FORWARD);
		DIRECTION_LOOKUP.put("b", Direction.REVERSE);
		DIRECTION_LOOKUP.put("l", Direction.SPIN_LEFT);
		DIRECTION_LOOKUP.put("r", Direction.SPIN_RIGHT);
		DIRECTION_LOOKUP.put("fl", Direction.FORWARD_LEFT);
		DIRECTION_LOOKUP.put("fr", Direction.FORWARD_RIGHT);
		DIRECTION_LOOKUP.put("bl", Direction.REVERSE_LEFT);
		DIRECTION_LOOKUP.put("br", Direction.REVERSE_RIGHT);
	}

	private final BlockingQueue<Command> commandQueue;

	public NetworkClient(BlockingQueue<Command> commandQueue) {
		this.commandQueue = commandQueue;
	}

	@Override
	public void run() {
		Log.i(TAG, "Trying to connect to " + Config.SERVER_HOST + ":" + Config.SERVER_PORT);
		try (Socket socket = new Socket(Config.SERVER_HOST, Config.SERVER_PORT); Scanner sc = new Scanner(socket.getInputStream())) {
			Log.i(TAG, "Connected: " + socket);
			while (sc.hasNext()) {
				String directionString = sc.next();
				float speed = sc.nextFloat();
				long duration = sc.nextLong();
				Direction direction = DIRECTION_LOOKUP.get(directionString);
				if (direction != null && duration != 0) {
					Command command = new Command(direction, speed, duration);
					Log.i(TAG, "Got command: " + command);
					commandQueue.put(command);
					Log.i(TAG, "Command queue length now " + commandQueue.size());
				} else {
					Log.w(TAG, "Invalid command: " + directionString + "/" + speed + "/" + duration);
				}
			}
		} catch (IOException | InterruptedException e) {
			Log.e(TAG, "Error talking to server", e);
		}
	}
}
