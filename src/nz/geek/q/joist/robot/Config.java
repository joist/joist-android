package nz.geek.q.joist.robot;

public class Config {
	public static final int LEFT_ENABLE_PIN = 1;
	public static final int LEFT_FORWARD_PIN = 2;
	public static final int LEFT_REVERSE_PIN = 3;
	public static final int RIGHT_ENABLE_PIN = 6;
	public static final int RIGHT_FORWARD_PIN = 5;
	public static final int RIGHT_REVERSE_PIN = 4;

	/** Frequency (in Hz) of PWM signal sent to motor enable pins. */
	public static final int MOTOR_PWM_FREQUENCY = 10000;

	public static final String SERVER_HOST = "192.168.1.154";
	public static final int SERVER_PORT = 10157;
}
