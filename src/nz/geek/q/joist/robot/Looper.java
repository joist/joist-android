package nz.geek.q.joist.robot;

import static nz.geek.q.joist.robot.Direction.NEUTRAL;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;

import java.util.concurrent.BlockingQueue;

public class Looper extends BaseIOIOLooper {
	private Robot robot;
	private DigitalOutput led;
	private final BlockingQueue<Command> commandQueue;

	public Looper(BlockingQueue<Command> commandQueue) {
		this.commandQueue = commandQueue;
	}

	@Override
	protected void setup() throws ConnectionLostException {
		robot = new Robot(ioio_);
		led = ioio_.openDigitalOutput(0, true);
	}

	@Override
	public void loop() throws ConnectionLostException, InterruptedException {
		// LED is active-low, so this turns it on
		led.write(false);

		// Wait for a command, then execute it
		Command command = commandQueue.take();
		led.write(true);
		robot.drive(command.direction, command.speed);
		Thread.sleep(command.duration);
		robot.drive(NEUTRAL, 0);
	}
}
