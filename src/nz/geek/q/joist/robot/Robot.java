package nz.geek.q.joist.robot;

import static nz.geek.q.joist.robot.Config.LEFT_ENABLE_PIN;
import static nz.geek.q.joist.robot.Config.LEFT_FORWARD_PIN;
import static nz.geek.q.joist.robot.Config.LEFT_REVERSE_PIN;
import static nz.geek.q.joist.robot.Config.RIGHT_ENABLE_PIN;
import static nz.geek.q.joist.robot.Config.RIGHT_FORWARD_PIN;
import static nz.geek.q.joist.robot.Config.RIGHT_REVERSE_PIN;
import ioio.lib.api.IOIO;
import ioio.lib.api.exception.ConnectionLostException;

public class Robot {
	private final Motor leftWheel;
	private final Motor rightWheel;

	public Robot(IOIO ioio) throws ConnectionLostException {
		this.leftWheel = new Motor(ioio, LEFT_ENABLE_PIN, LEFT_FORWARD_PIN, LEFT_REVERSE_PIN);
		this.rightWheel = new Motor(ioio, RIGHT_ENABLE_PIN, RIGHT_FORWARD_PIN, RIGHT_REVERSE_PIN);
	}

	/**
	 * Drive in the given direction at the given speed, until another command is given.
	 */
	public void drive(Direction direction, float speed) throws ConnectionLostException {
		switch (direction) {
		case NEUTRAL:
			leftWheel.neutral();
			rightWheel.neutral();
			break;
		case BRAKE:
			leftWheel.brake();
			rightWheel.brake();
			break;
		case FORWARD:
			leftWheel.turn(true, speed);
			rightWheel.turn(true, speed);
			break;
		case REVERSE:
			leftWheel.turn(false, speed);
			rightWheel.turn(false, speed);
			break;
		case SPIN_LEFT:
			leftWheel.turn(false, speed);
			rightWheel.turn(true, speed);
			break;
		case SPIN_RIGHT:
			leftWheel.turn(true, speed);
			rightWheel.turn(false, speed);
			break;
		case FORWARD_LEFT:
			leftWheel.brake();
			rightWheel.turn(true, speed);
			break;
		case FORWARD_RIGHT:
			leftWheel.turn(true, speed);
			rightWheel.brake();
			break;
		case REVERSE_LEFT:
			leftWheel.brake();
			rightWheel.turn(false, speed);
			break;
		case REVERSE_RIGHT:
			leftWheel.turn(false, speed);
			rightWheel.brake();
			break;
		}
	}
}
