package nz.geek.q.joist.robot;

public enum Direction {
	NEUTRAL,
	BRAKE,
	FORWARD,
	REVERSE,
	SPIN_LEFT,
	SPIN_RIGHT,
	FORWARD_LEFT,
	FORWARD_RIGHT,
	// Reversing turns the robot in the opposite direction to turning forwards or spinning
	REVERSE_LEFT,
	REVERSE_RIGHT
}
